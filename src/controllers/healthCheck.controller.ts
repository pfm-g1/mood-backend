import { Request, Response } from "express";

export default class HealthCheckController {
  public static check(request: Request, response: Response): Response<boolean> {
    return response.status(200).send(true);
  }
}
