import express from "express";

import HealthCheckController from "../controllers/healthCheck.controller";

const router = express.Router();

router.post("/health-check", HealthCheckController.check);

export default router;
