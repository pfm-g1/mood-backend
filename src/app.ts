import express from "express";
import logger from "morgan";

import cors from "./middlewares/cors.middleware";
import router from "./routers/router";

const application = express();

application.use(cors);
application.use(logger("dev"));
application.use(express.json());
application.use(express.urlencoded({ extended: false }));

application.use("/", router);

// catch 404 and forward to error handler
// application.use(notFoundMiddleware);
// application.use(fallbackErrorHandler);

export default application;
