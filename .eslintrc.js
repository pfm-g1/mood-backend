module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended', // Uses the recommended rules from the @typescript-eslint/eslint-plugin
  ],
  env: {
    es6: true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: ['@typescript-eslint'],
  rules: {
    '@typescript-eslint/no-unused-vars': ['warn'],
    '@typescript-eslint/indent': ['error', 2],
    '@typescript-eslint/no-var-requires': ['off'],
    '@typescript-eslint/no-non-null-assertion': ['off'],
    '@typescript-eslint/no-explicit-any': ['off'],
    "indent": [
      "error",
      2,
      {
        "ignoredNodes": ["TemplateLiteral"],
        "SwitchCase": 1
      }
    ],
    quotes: ['error', 'double'],
    semi: ['error', 'always'],
    curly: ['error', 'all'],
    'no-return-await': ['error'],
    'keyword-spacing': ['error', { before: true, after: true }],
    'no-multi-spaces': ['error'],
    'no-trailing-spaces': ['error'],
    'object-curly-spacing': ['error', 'always'],
    'array-bracket-spacing': ['error', 'never'],
    'space-before-blocks': ['error', 'always'],
    'space-before-function-paren': [
      'error',
      { anonymous: 'never', asyncArrow: 'always', named: 'never' },
    ],
    'max-params': ['error', 4],
    'comma-dangle': ['warn', 'only-multiline'],
    camelcase: ['warn'],
    'no-var': ['error'],
    '@typescript-eslint/interface-name-prefix': ['off'],
  },
};
